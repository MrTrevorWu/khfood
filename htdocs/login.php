<?php
session_start();
include_once('sql.inc.php');

function checkText($val)
{
    return (strlen($val) > 0);
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (checkText($_POST['userName'])
        && checkText($_POST['userMail'])
        && checkText($_POST['userPassword'])) {
        $userNames = mysqli_real_escape_string($conn, $_POST['userName']);
        $userName = htmlspecialchars($userNames, ENT_QUOTES, 'UTF-8');
        $userMails = mysqli_real_escape_string($conn, $_POST['userMail']);
        $userMail = htmlspecialchars($userMails, ENT_QUOTES, 'UTF-8');
        $userPasswords = mysqli_real_escape_string($conn, $_POST['userPassword']);
        $userPassword = htmlspecialchars($userPasswords, ENT_QUOTES, 'UTF-8');

        // Check user's mail already exists or not.
        $sql = "SELECT * FROM `user` WHERE `userMail`=\"$userMail\"";
        $result = mysqli_query($conn, $sql);
        while ($arr = mysqli_fetch_array($result)) $checkmails[] = $arr;

        if (isset($checkmails) && count($checkmails) > 0) {
            // Login into exist account
            $sqlFetchData = "SELECT * FROM `user` WHERE `userMail`=\"$userMail\" AND `userPassword`=\"$userPassword\"";
            $result = mysqli_query($conn, $sqlFetchData);
            while ($arr = mysqli_fetch_array($result)) $userInfo[] = $arr;
            if (isset($userInfo) && count($userInfo) > 0) {
                // Login Success
                $_SESSION['userId'] = $userInfo[0]['userId'];
                $_SESSION['userName'] = $userInfo[0]['userName'];
                header("Location: index.php");
            } else {
                // Login failed
                $err_msg = "登入資訊錯誤";
            }

        } else if (count($checkmails) == 0) {
            // Create a new account.
            $sql = "INSERT INTO `user`(`userName`, `userMail`, `userPassword`) VALUES (\"$userName\",\"$userMail\",\"$userPassword\")";
            mysqli_query($conn, $sql);

            // Get register info from DB and login in.
            $sqlFetchData = "SELECT * FROM `user` WHERE `userMail`=\"$userMail\"";
            $result = mysqli_query($conn, $sqlFetchData);
            while ($arr = mysqli_fetch_array($result)) $userInfo[] = $arr;

            $_SESSION['userId'] = $userInfo[0]['userId'];
            $_SESSION['userName'] = $userInfo[0]['userName'];
            header("Location: index.php");
        }
    } else {
        $err_msg = "錯誤，請檢查填寫資訊";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>~ 高雄十大美食 ~</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/grayscale.min.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">
</head>
<body class="randomBackground">
<div class="container d-flex h-100">
    <div class="row align-self-center w-100">
        <div class="col-sm-6 mx-auto">
            <form class="formBorder formPadding" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>"
                  enctype="multipart/form-data">
                <a href="../">< 返回</a>
                <hr>
                <div class="alert alert-info" role="alert">
                    註冊：<br>
                    依序填入資訊即可。<br>
                    登入：<br>
                    填入電子郵電和密碼，暱稱隨意填寫即可。<br>
                </div>
                <hr>
                <?php if (isset($err_msg)) { ?>
                    <div class="alert alert-danger" role="alert">
                        <?= $err_msg ?>
                    </div>
                <?php } ?>
                <div class="form-group">
                    <label for="userName">暱稱</label>
                    <input type="text" name="userName" id="userName" class="form-control" placeholder=""
                           aria-describedby="helpId">
                </div>
                <div class="form-group">
                    <label for="userMail">電子郵件</label>
                    <input type="email" name="userMail" id="userMail" class="form-control" placeholder=""
                           aria-describedby="helpId">
                </div>
                <div class="form-group">
                    <label for="userPassword">密碼</label>
                    <input type="password" name="userPassword" id="userPassword" class="form-control" placeholder=""
                           aria-describedby="helpId">
                </div>
                <button type="submit" class="btn btn-primary" style="width: 100%; margin-top: 8px;">登入 / 註冊</button>
            </form>
        </div>
    </div>
</div>
</body>