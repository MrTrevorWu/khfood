<?php
session_start();
include_once('sql.inc.php');

global $foodId;
global $foodDetails;
global $userId;
$userId = $_SESSION['userId'];

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    if (!isset($_SESSION['userId'])) {
        header("Location: login.php");
    }
    $_SESSION['foodId'] = $_GET['id'];
    $foodId = $_GET['id'];

    // Fetch from database.
    $sql = "SELECT * FROM `foodDetails` WHERE `id`= $foodId";
    $result = mysqli_query($conn, $sql);
    while ($arr = mysqli_fetch_array($result)) $foodDetails[] = $arr;

    // Fetch avg rating from database.
    $sqlFoodRatingAvg = "SELECT AVG(`rate`)
                    FROM `rating`
                    WHERE rating.foodId = $foodId";
    $foodRatingAvgResult = mysqli_query($conn, $sqlFoodRatingAvg);
    while ($arr = mysqli_fetch_array($foodRatingAvgResult)) $foodRatingAvgArray[] = $arr;

    $foodRatingAvgToShow = 0;
    if (!empty($foodRatingAvgArray)) {
        $foodRatingAvgToShow = $foodRatingAvgArray[0]["AVG(`rate`)"];
    }
    $foodRatingAvgToShow = substr($foodRatingAvgToShow, 0, 3);

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">

    <!-- Custom styles for this template -->
    <!-- <link href="css/grayscale.min.css" rel="stylesheet"> -->
</head>
<body>

<div class="container" style="padding-top: 16px; padding-bottom: 16px">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?= $foodDetails[0]['foodTitle'] ?></li>
        </ol>
    </nav>

    <!-- Food Project Row -->
    <div class="row align-items-center no-gutters mb-4 mb-lg-5">

        <div class="col-xl-8 col-lg-7">

            <?php if ($foodDetails[0]['foodImage'] != "") { ?>
                <img src="img/<?= $foodDetails[0]['foodImage'] ?>" class="img-fluid mb-3 mb-lg-0" style="border: 0;">
            <?php } else { ?>
                <img src="https://fakeimg.pl/680x460" class="img-fluid mb-3 mb-lg-0" style="border: 0;">
            <?php } ?>
        </div>

        <div class="col-xl-4 col-lg-5">
            <div class="featured-text text-center text-lg-left">
                <h4><?= $foodDetails[0]['foodTitle'] ?></h4>
                <h5>位於 <b><?= $foodAreaText[$foodDetails[0]['foodArea']] ?></b></h5>
                <h6>綜合評價 <b><?= $foodRatingAvgToShow ?></b></h6>
                <hr>
                <p class="text-black-50 mb-0"><?= $foodDetails[0]['foodDescribe'] ?></p>
            </div>
        </div>

    </div>

    <h4>他人留言：</h4>

    <div class="row">
        <?php
        $sql = "SELECT `comment`, `userName` 
                    FROM `comments`, `user` 
                    WHERE `foodId`= $foodId
                    AND comments.userId = user.userId";
        $result = mysqli_query($conn, $sql);
        global $foodComments;
        while ($arr = mysqli_fetch_array($result)) $foodComments[] = $arr;
        ?>

        <div class="col-sm-6">
            <?php
            if (!empty($foodComments)) {
                for ($i = 0; $i < count($foodComments); $i++) { ?>
                    <div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title"><?= $foodComments[$i]['comment'] ?></h5>
                            <p class="card-text">- <?= $foodComments[$i]['userName'] ?></p>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>


    </div>

    <hr>

    <div class="row">
        <div class="col-sm-6">
            <h4>留言</h4>
            <form class="" method="POST" action="newComment.php" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="foodComment">留言訊息</label>
                    <input type="text" name="foodComment" id="foodComment" class="form-control" placeholder=""
                           aria-describedby="helpId">
                </div>

                <button type="submit" class="btn btn-primary" style="width: 100%; margin-top: 8px;">送出</button>

            </form>
        </div>

        <div class="col-sm-6">
            <?php
            $sql2 = "SELECT `rate`
                    FROM `rating`
                    WHERE rating.foodId = $foodId
                    AND rating.userId = $userId";
            $result2 = mysqli_query($conn, $sql2);
            while ($arr2 = mysqli_fetch_array($result2)) $foodRating[] = $arr2;

            $userRatingForDisplay = 0;
            if (!empty($foodRating)) {
                $userRatingForDisplay = $foodRating[0]["rate"];
            }
            ?>
            <h4>評分</h4>
            <form class="" method="POST" action="newRating.php" enctype="multipart/form-data">

                <div class="form-group">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="radioInline1" name="foodRating" class="custom-control-input"
                               value="1" <?php if ($userRatingForDisplay == 1) echo "checked" ?>>
                        <label class="custom-control-label" for="radioInline1">超爛</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="radioInline2" name="foodRating" class="custom-control-input"
                               value="2" <?php if ($userRatingForDisplay == 2) echo "checked" ?>>
                        <label class="custom-control-label" for="radioInline2">有點爛</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="radioInline3" name="foodRating" class="custom-control-input"
                               value="3" <?php if ($userRatingForDisplay == 3) echo "checked" ?>>
                        <label class="custom-control-label" for="radioInline3">中庸</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="radioInline4" name="foodRating" class="custom-control-input"
                               value="4" <?php if ($userRatingForDisplay == 4) echo "checked" ?>>
                        <label class="custom-control-label" for="radioInline4">美中不足</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="radioInline5" name="foodRating" class="custom-control-input"
                               value="5" <?php if ($userRatingForDisplay == 5) echo "checked" ?>>
                        <label class="custom-control-label" for="radioInline5">超級推薦</label>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary" style="width: 100%; margin-top: 8px;">送出</button>

            </form>
        </div>
    </div>

</div><!-- ./container -->

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for this template -->
<!-- <script src="js/grayscale.min.js"></script> -->

<script>
    function goBack() {
        window.history.back();
    }
</script>

</body>
</html>