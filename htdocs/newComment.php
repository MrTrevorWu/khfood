<?php
    session_start();
    include_once ('sql.inc.php');
    
    function checkText($val) {
        return (strlen($val)>0);
    }

    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        $foodId = $_SESSION['foodId'];
        $userId = $_SESSION['userId'];
        
        if(checkText($_POST['foodComment'])) {
            $foodComments = mysqli_real_escape_string($conn, $_POST['foodComment']);
            $foodComment = htmlspecialchars($foodComments, ENT_QUOTES, 'UTF-8');

            // New comment.
            $sql = "INSERT INTO `comments`(`foodId`, `userId`, `comment`) VALUES (\"$foodId\",\"$userId\",\"$foodComment\")";
            mysqli_query($conn, $sql);

            header("Location: showFood.php?id=" . $foodId);
        } else {
            // Error, get back.
            header("Location: showFood.php?id=" . $foodId);
        }

    }
?>