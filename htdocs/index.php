<?php
session_start();
include_once 'sql.inc.php';

// Fetch from database.
$sql = "SELECT * FROM `foodDetails`";

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $foodAreaSearchs = mysqli_real_escape_string($conn, $_POST['foodAreaSearch']);
    $foodAreaSearch = htmlspecialchars($foodAreaSearchs, ENT_QUOTES, 'UTF-8');

    if ($foodAreaSearch == 0) {
        $sql = "SELECT * FROM `foodDetails`";
    } else {
        $sql = "SELECT * FROM `foodDetails` WHERE `foodArea` = $foodAreaSearch";
    }
}
$result = mysqli_query($conn, $sql);
while ($arr = mysqli_fetch_array($result)) $foodDetails[] = $arr;
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>高雄美食情報站</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/grayscale.min.css" rel="stylesheet">
</head>

<body id="page-top">
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Eat more and more fatter </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#about">首頁</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#foodList">食物列表</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#signup">關於我們</a>
                </li>

                <li class="nav-item">
                    <?php if (isset($_SESSION['userId']) && isset($_SESSION['userName'])) { ?>
                        <a class="nav-link js-scroll-trigger" href="./logout.php">嗨<?= $_SESSION['userName'] ?>, 登出</a>
                    <?php } else { ?>
                        <a class="nav-link js-scroll-trigger" href="./login.php">登入 / 註冊</a>
                    <?php } ?>
                </li>

            </ul>
        </div>
    </div>
</nav>

<!-- Header -->
<header class="masthead" id="about">
    <div class="container d-flex h-100 align-items-center">
        <div class="mx-auto text-center">
            <h1 class="mx-auto my-0 text-uppercase">高雄美食情報站</h1>
            <h2 class="text-white-50 mx-auto mt-2 mb-5">
                歡迎來到美食之都「高雄」<br>
                在這裡你可以找到美食，<br>
                也可以和大家分享你覺得超好吃的美食！
            </h2>
            <a href="#foodList" class="btn btn-primary js-scroll-trigger">開吃</a>
        </div>
    </div>
</header>

<!-- FoodList Section -->
<section id="foodList" class="projects-section bg-light">
    <div class="container">

        <?php if (!isset($_SESSION['userId'])) { ?>
            <div class="alert alert-danger text-center" role="alert">
                等等! 在您開始查看前請先登入!
            </div>
        <?php } ?>


        <div class="row">
            <div class="col-sm-12">
                <form class="form-inline" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>"
                      enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="foodArea">位置篩選</label>
                        <select class="form-control" id="foodArea" name="foodAreaSearch">
                                <option value="0">全部</option>
                            <?php for ($i = 2; $i < count($foodAreaText); $i++) { ?>
                                <option value="<?= $i ?>"><?= $foodAreaText[$i] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">搜尋</button>
                </form>
            </div>
        </div>


        <div class="row">
            <?php for ($i = 0; $i < count($foodDetails); $i++) { ?>
                <!-- Card Section -->
                <div class="col-sm-4">
                    <div class="card mb-3">
                        <?php if ($foodDetails[$i]['foodImage'] != "") { ?>
                            <img src="img/<?= $foodDetails[$i]['foodImage'] ?>" class="card-img-top" style="border: 0;">
                        <?php } else { ?>
                            <img src="https://fakeimg.pl/680x460" class="card-img-top" style="border: 0;">
                        <?php } ?>
                        <div class="card-body">
                            <h5 class="card-title"><?= $foodDetails[$i]['foodTitle'] ?></h5>
                            <h6 class="card-title"><?= $foodAreaText[$foodDetails[$i]['foodArea']] ?></h6>
                            <p class="card-text"><?= $foodDetails[$i]['foodDescribe'] ?></p>
                            <a href="showFood.php?id=<?= $foodDetails[$i]['id'] ?>" class="btn btn-primary">查看</a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="row justify-content-center">
            <div class="col-4">
                <a href="addFood.php" class="btn btn-primary">漏了什麼嗎? 推薦食物!</a>
            </div>
        </div>

    </div><!-- ./container -->
</section>

<!-- Signup Section -->
<section id="signup" class="signup-section">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-8 mx-auto text-center">

                <i class="far fa-paper-plane fa-2x mb-2 text-white"></i>
                <h2 class="text-white mb-5">關於我們</h2>

                <p class="text-white">
                    一群超愛吃東西的高雄人，因為實在太多美食了，所以整理起來與大家一同分享。
                </p>

            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<footer class="bg-black small text-center text-white-50">
    <div class="container">
        Made with &#x2764; in KH
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for this template -->
<script src="js/grayscale.min.js"></script>

</body>

</html>
