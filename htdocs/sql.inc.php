<?php
    // Set time zone
    date_default_timezone_set("Asia/Taipei");

    // Conference Name
    $title_name = "~ 高雄十大美食 ~";

    // Database Location
    $db_server  = "localhost";

    // Database Name
    $db_name    = "10food";

    // Database Username
    $db_user    = "10food";

    // Database Password
    $db_passwd  = "irN26De6ysk9tnvs";

    // Set default encoding order
    mb_detect_order('BIG-5');

    $foodAreaText = array("未知", "多處", "鹽埕區", "鼓山區", "左營區", "楠梓區", "三民區", "新興區", "前金區", "苓雅區", "前鎮區", "旗津區", "小港區",
    "鳳山區", "林園區", "大寮區", "大樹區", "大社區", "仁武區", "鳥松區", "岡山區", "橋頭區", "燕巢區", "田寮區", "阿蓮區", "路竹區",
    "湖內區", "茄萣區", "永安區", "彌陀區", "梓官區", "旗山區", "美濃區", "六龜區", "甲仙區", "杉林區", "內門區", "茂林區", "桃源區",
    "那瑪夏區");

    // Connect to DB. For security reason, show funny message to user, otherwise everyone will know you can't connect to DB.
    $conn = @mysqli_connect($db_server, $db_user, $db_passwd, $db_name);
    if(mysqli_connect_errno($conn))
        die('網站發生技術性問題，我們已經派出猴子去修復了！');
    mysqli_set_charset($conn, "utf8");
?>