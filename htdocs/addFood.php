<?php
session_start();
include_once 'sql.inc.php';

function checkText($val)
{
    return (strlen($val) > 0);
}

global $foodId;
global $foodDetails;
global $userId;

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    if (!isset($_SESSION['userId'])) {
        header("Location: login.php");
    }
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $userId = $_SESSION['userId'];

    if (checkText($_POST['foodArea']) &&
        isset($_FILES['shopImg']) &&
        checkText($_POST['foodTitle']) &&
        checkText($_POST['foodDescribe'])
    ) {
        $foodAreas = mysqli_real_escape_string($conn, $_POST['foodArea']);
        $foodArea = htmlspecialchars($foodAreas, ENT_QUOTES, 'UTF-8');
        $foodTitles = mysqli_real_escape_string($conn, $_POST['foodTitle']);
        $foodTitle = htmlspecialchars($foodTitles, ENT_QUOTES, 'UTF-8');
        $foodDescribes = mysqli_real_escape_string($conn, $_POST['foodDescribe']);
        $foodDescribe = htmlspecialchars($foodDescribes, ENT_QUOTES, 'UTF-8');
        $foodImgName = $_FILES['shopImg']['name'];

        // New food.
        $sql = "INSERT INTO `fooddetails`(`userId`, `foodArea` ,`foodImage`, `foodTitle`, `foodDescribe`)
                VALUES (\"$userId\", \"$foodArea\", \"$foodImgName\", \"$foodTitle\",\"$foodDescribe\")";
        mysqli_query($conn, $sql);

        // 從資料庫抓取該食物 id
        // $sqlFetchData = "SELECT * FROM `fooddetails` ORDER BY id DESC";
        // $result = mysqli_query($conn, $sqlFetchData);
        // while ($arr = mysqli_fetch_array($result)) {
        //     $foods[] = $arr;
        // }

        // 檔名重複會直接蓋檔，Demo 時要注意
        move_uploaded_file($_FILES["shopImg"]["tmp_name"], "./img/" . $_FILES['shopImg']['name']);

        header("Location: index.php");
    } else {
        // Error, get back.
        header("Location: addFood.php");
    }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <!-- <link href="css/grayscale.min.css" rel="stylesheet"> -->
    <style>
        .myCustomCard {
            border-top: 6px solid #40A4F4;
            -webkit-box-shadow: 0 4px 6px 0 hsla(0, 0%, 0%, 0.2);
            -moz-box-shadow: 0 4px 6px 0 hsla(0, 0%, 0%, 0.2);
            box-shadow: 0 4px 6px 0 hsla(0, 0%, 0%, 0.2);
        }
    </style>
</head>

<body>
<div class="container" style="padding-top: 16px; padding-bottom: 16px">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
            <li class="breadcrumb-item active" aria-current="page">新增食物</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-sm-6">
            <p>
                有什麼你覺得超讚的食物大家也不能錯過的嗎? 填寫右側表單即可在網站上新增你覺得超讚的食物囉!
            </p>
        </div>

        <div class="col-sm-6">
            <div class="card p-4 myCustomCard">
                <h4 class="text-center">推薦新食物</h4>
                <hr>
                <form class="" method="POST" action="<?=$_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">

                    <div class="form-group">
                        <label for="foodArea">店家位置</label>
                        <select class="form-control" id="foodArea" name="foodArea">
                            <?php for ($i = 2; $i < count($foodAreaText); $i++) { ?>
                                    <??>
                                    <option value="<?=$i?>"><?= $foodAreaText[$i] ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlFile1">圖片上傳</label>
                        <input type="file" class="form-control-file"  id="shopImg" name="shopImg">
                    </div>

                    <div class="form-group">
                        <label for="foodTitle">店家名稱</label>
                        <input type="text" name="foodTitle" class="form-control" placeholder="" aria-describedby="helpId">
                    </div>

                    <div class="form-group">
                        <label for="foodDescribe">簡介</label>
                        <textarea type="text" name="foodDescribe" class="form-control" placeholder="" aria-describedby="helpId" rows="3"></textarea>
                    </div>

                    <button type="submit" class="btn btn-primary" style="width: 100%; margin-top: 8px;">送出</button>

                </form>
            </div><!-- ./card -->
        </div>
    </div>




</div><!-- ./container -->

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <!-- <script src="js/grayscale.min.js"></script> -->

</body>
</html>