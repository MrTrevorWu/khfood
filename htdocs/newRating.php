<?php
session_start();
include_once('sql.inc.php');

function checkText($val)
{
    return (strlen($val) > 0);
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $foodId = $_SESSION['foodId'];
    $userId = $_SESSION['userId'];

    if (checkText($_POST['foodRating'])) {
        $foodRatings = mysqli_real_escape_string($conn, $_POST['foodRating']);
        $foodRating = htmlspecialchars($foodRatings, ENT_QUOTES, 'UTF-8');

        $sql = "SELECT `rate` 
                FROM `rating` 
                WHERE rating.foodId = $foodId 
                AND rating.userId = $userId";
        $result = mysqli_query($conn, $sql);
        while ($arr = mysqli_fetch_array($result)) $foodRatingSQL[] = $arr;
        if (empty($foodRatingSQL)) {
            // New rating
            $sql = "INSERT INTO `rating`(`foodId`, `userId`, `rate`) 
                    VALUES (\"$foodId\",\"$userId\",\"$foodRating\")";
        } else {
            // Update rating
            $sql = "UPDATE `rating`
                    SET rate = $foodRating
                    WHERE rating.foodId = $foodId 
                    AND rating.userId = $userId";
        }
        mysqli_query($conn, $sql);

        header("Location: showFood.php?id=" . $foodId);
    } else {
        // Error, get back.
        header("Location: showFood.php?id=" . $foodId);
    }

}
?>