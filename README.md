# 高雄美食情報站

## 環境需求
直接安裝 XAMPP

## 環境設定
### 啟動伺服器
1. 前往 `C:\xampp` 並開啟 `xampp-control.exe`
2. 找到 `Apache` 和 `SQL` 並選擇 `Start`
 
### 資料庫設定
1. 開啟 [localhost/phpmyadmin](localhost/phpmyadmin) 連線到資料庫後台
2. 將以下路徑的資料庫檔案匯入
```
./sql/10food.sql
```

### 網站本體設定
將 `路徑 1` 的所有檔案放入 `路徑 2`中
```
# 路徑 1
./htdocs/

# 路徑 2
C:\xampp\htdocs
```