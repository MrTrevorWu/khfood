-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 2020-11-21 03:01:46
-- 伺服器版本： 10.4.13-MariaDB
-- PHP 版本： 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `10food`
--

-- --------------------------------------------------------

--
-- 資料表結構 `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `foodId` text NOT NULL,
  `userId` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `comments`
--

INSERT INTO `comments` (`id`, `foodId`, `userId`, `comment`) VALUES
(3, '2', '2', 'CP爆高 超推'),
(4, '2', '2', '食物超棒 物美價廉'),
(5, '1', '2', '真的美味'),
(6, '1', '4', '好吃'),
(7, '1', '4', '狗幹好吃'),
(8, '3', '1', 'kjalksdjklasdjkaldjlasdjlaks'),
(10, '11', '1', 'test 11');

-- --------------------------------------------------------

--
-- 資料表結構 `fooddetails`
--

CREATE TABLE `fooddetails` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `foodArea` int(11) NOT NULL,
  `foodImage` text NOT NULL,
  `foodTitle` text NOT NULL,
  `foodDescribe` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `fooddetails`
--

INSERT INTO `fooddetails` (`id`, `userId`, `foodArea`, `foodImage`, `foodTitle`, `foodDescribe`) VALUES
(1, 0, 10, 'food1.jpg', '韓式料理-玉豆腐', '近年來韓劇興起，大家追韓流，都跟隨電視開始吃起韓式料理，當然這股韓流也吹來南部的高雄，而高雄人最挺高雄人，吃韓式料理必吃高雄最好吃的玉豆腐，店家是高雄起家，遠赴韓國學習，集結韓國的味覺記憶 呈現韓國在地原味，韓國料理經典代表「豆腐煲」，每日現作手工豆腐，真的不是蓋的，綿密滑嫩的口感，讓您一吃就愛上，還有一道 明太子起司烘蛋捲～香濃好吃，每桌必上五樣道地韓式小菜，這也是師傅每天以新鮮時令食材製作，非常美味下飯，重點是完全免費，老闆很佛心還可以免費續加，不怕你吃'),
(2, 0, 8, 'food2.jpg', '河邊海鮮-港都海鮮', '河邊海鮮，高雄人都知道的老店，屹立不搖叱吒風雲高雄近50年，通常有山的城市，不一定會有海 ，何況高雄是一個山、海、河、市共鎮的城市 ，離海港相當近，附有豐富的海鮮資源，大家都說，高雄靠海~，海鮮最新鮮、最便宜也最能代表高雄，高雄最早起家的就是海鮮，嚴選高雄最新鮮海鮮食材，搭配主廚近50年的好手藝，每一道料理都讓人讚不絕口，乾煸胡椒鳳螺、手工黑鮪魚香腸、紅燒海鮮羹，這幾道菜色，是到河邊必吃的招牌菜，招待家人或是朋友都很有面子的海鮮餐廳，重大節日帶重要的貴賓來吃也是十足的滿意。'),
(3, 0, 2, 'food3.jpg', '翰品港飲-港式飲茶', '翰品酒店港都茶樓，前身為中信飲茶，是全高雄歷史悠久、最火紅的港飲，高雄人愛吃港式飲茶是最出名，高雄每家飯店基本上都有港式飲茶餐廳，常常也是團購排行版第一名的類型，翰品酒店港都茶樓，近年來一直不斷更新，點心都是師傅每天早上現做的，並循傳統推車式的方式呈現，延續在地老字號港式茶樓特色的茶餐廳，陪伴高雄人走過數十個年頭，也是高雄美食裡面必吃的經典，最推薦金乳豬拼盤、黃金流沙包、鮮蝦腸粉，道道都是必點的港飲招牌。'),
(4, 0, 8, 'food4.jpg', '右京棧-酒粕火鍋', '南部人、高雄人都是有名的愛吃火鍋，高雄火鍋不得不推薦最特別的右京棧-大吟釀の鍋，全台灣唯一一家絕無分店，採用日本清酒頂級大吟釀，店內主打四種招牌酒粕Pitera鍋底，酒粕香辣鍋、吟釀麻辣鍋、酸白酒粕鍋、膠原吟釀鍋，四種鍋物滿足不同的味蕾，Pitera女生一定有聽過，就是skii的招牌青春露中的Pitera，其中含有珍貴Pitera酵母成分，也讓麻辣火鍋，多了一份酒粕的香甜味，火鍋的價值，很難定義，但湯頭的精華是右京棧堅持米心極粹的珍貴，不藏私的將青春Pitera，好食材溫潤地傳遞給家人、好友，這份心、這份情，40年後或許時空轉換，但藏人究極工序依舊不變，手做的最難得，也是最美好的味道。'),
(5, 0, 5, 'food5.jpg', '小吃-江豪記臭豆腐', '高雄的臭豆腐在全台灣是有名氣的，高雄喜歡整合式，一路到底都是臭豆腐入菜，有臭豆腐火鍋、臭豆腐餃、臭豆腐春捲、臭豆腐包子，而 高雄臭豆腐，最出名的絕對是老字號「江豪記」，連外國朋友到高雄～，在地人的我們都帶來吃江豪記臭豆腐，蒸式臭豆腐，特別受人喜愛，一口湯汁、一塊小粉蒸肉、一口小蔥搭配臭豆腐，特別的爽口多汁~試一試你就會愛上，料理非常的多樣化也十足創意，辣度可隨人調整，從不辣到大辣都可嘗試，雖然是小吃但內裝潢可不馬虎，有時尚感的花做裝飾，備感尊榮。'),
(6, 0, 6, 'food6.jpg', '家常便當- 正忠排骨飯', '高雄便當店，正忠排骨飯算是高雄之光了\r\n            起源於高雄市，創立於1991年\r\n            以在地人的口味，出外人的便當為初衷理念\r\n            一塊大大的排骨滿足高雄人的味蕾\r\n            \r\n            高雄無人不知無人不曉是高雄的名店之一\r\n            也是餐飲業中便當界龍頭\r\n            來到高雄一定要嚐嚐看這傳奇便當\r\n            \r\n            不論是烤雞腿、炸雞腿、排骨等便當\r\n            都深受老少青三代的喜愛\r\n            從小孩到大人都愛的便當'),
(7, 0, 6, 'food7.jpg', '平民美食-佑佑鍋燒麵', '高雄人都不陌生「在地人會吃的店」\r\n            食尚玩家也推薦報導過\r\n            \r\n            高雄人愛吃鍋燒麵是出名的\r\n            不管是早餐、到午餐、晚餐、到宵夜都要吃鍋燒麵\r\n            \r\n            雖然是小吃，但有舒適環境\r\n            有冷氣可吹、份量又大，真材實料，食材新鮮\r\n            鍋燒麵，首重就是湯頭、還有一定會煮到剛剛好的蛋\r\n            佑佑的鍋燒麵，湯頭好，還有4隻大蝦，裡面不放一班火鍋料\r\n            好吃又健康，每次湯都會喝到一滴不剩\r\n            這裡 還供應免費汽水、冰開水喝到飽，是夏天最佳的好朋友\r\n            簡單用心的好美味\r\n            親民價格美味更是大推薦'),
(8, 0, 13, 'food8.jpg', '酥炸雞排-天使雞排', '高雄新起家 最夯最火紅的厚切大雞排\r\n            愛吃雞排的人說，台中惡魔、高雄天使\r\n            高雄人都知道天使雞排是在地高雄起家\r\n            這雞排允指誘人的好味道，只有吃過才知道\r\n            天使的脆皮較薄、較脆，雞肉鮮嫩多汁\r\n            厚度大概有4cm左右吧，超誇張的\r\n            大口大口的咬下，嘴裡滿是雞排的湯汁\r\n            每一口都好滿足，就是一個「爽」字\r\n            皮薄、鮮嫩、多汁呀\r\n            皮很酥脆、又香，而且不會油膩'),
(9, 0, 6, 'food9.jpg', '消暑冰品-大碗公冰', '高雄人最怕熱也最愛吃冰品\r\n            店成立於1995年\r\n            以天下第一大碗公剉冰聞名國內外\r\n            \r\n            老闆就是想要傳達一個分享的觀念\r\n            推出2、4、6、10、30倍大碗公剉冰\r\n            挑戰杯(1000cc)、世界杯(5000cc)冷飲等等\r\n            \r\n            堅持採用新鮮香甜的水果，遵循古法熬煮的黑糖漿水\r\n            符合衛生標準的冰磚，精緻的食材與澎拜的份量\r\n            \r\n            最推薦的絕對是紅豆芋頭牛奶冰，那綿綿甜甜的紅豆芋頭\r\n            從小朋友到大朋友都會喜歡'),
(10, 0, 6, 'food10.jpg', '經典飲料-老江紅茶', '老江紅茶創立於民國1953年\r\n            是高雄人小時候記憶中最懷念的古早味\r\n            高雄最知名飲品、早餐、宵夜專賣店\r\n            陪伴高雄人走過一甲子的黃金歲月\r\n            深受高雄人的喜愛，被食尚玩家採訪三次\r\n            \r\n            老江紅茶最暢銷的經典飲品就是紅茶牛奶\r\n            紅茶遵循家傳秘方冰鎮古法製作而成\r\n            再加入一整罐新鮮牧高牛奶\r\n            傳統的好飲品就是歷久不衰\r\n            \r\n            搭配招牌火腿蛋土司就是絕配\r\n            從早餐到午餐到晚餐到宵夜通通都有提供\r\n            絕對是高雄人必吃的好滋味'),
(11, 1, 2, '', 'test', 'test'),
(12, 1, 3, '', '吳家紅茶冰', '狗幹便宜又好喝');

-- --------------------------------------------------------

--
-- 資料表結構 `rating`
--

CREATE TABLE `rating` (
  `id` int(11) NOT NULL,
  `foodId` int(11) NOT NULL DEFAULT 0,
  `userId` int(11) NOT NULL DEFAULT 0,
  `rate` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `rating`
--

INSERT INTO `rating` (`id`, `foodId`, `userId`, `rate`) VALUES
(1, 12, 1, 2),
(6, 11, 1, 1),
(7, 11, 2, 5),
(8, 1, 1, 4);

-- --------------------------------------------------------

--
-- 資料表結構 `user`
--

CREATE TABLE `user` (
  `userId` int(11) NOT NULL,
  `userName` text NOT NULL,
  `userMail` text NOT NULL,
  `userPassword` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `user`
--

INSERT INTO `user` (`userId`, `userName`, `userMail`, `userPassword`) VALUES
(1, '馬英九', 'aaa@aaa.aaa', 'aaa'),
(2, '陳家祥', 'bbb@bbb.bbb', 'bbb'),
(4, '蔡英文', 'ccc@ccc.ccc', 'ccc'),
(5, 'a', 'aaa@aaa.aa', 'aaa');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `fooddetails`
--
ALTER TABLE `fooddetails`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userId`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `fooddetails`
--
ALTER TABLE `fooddetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `user`
--
ALTER TABLE `user`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
